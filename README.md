# OpenML dataset: Zombies-Apocalypse

https://www.openml.org/d/43786

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
News reports suggest that the impossible has become possiblezombies have appeared on the streets of the US! What should we do? The Centers for Disease Control and Prevention (CDC) zombie preparedness website recommends storing water, food, medication, tools, sanitation items, clothing, essential documents, and first aid supplies. Thankfully, we are CDC analysts and are prepared, but it may be too late for others!

Content
Our team decides to identify supplies that protect people and coordinate supply distribution. A few brave data collectors volunteer to check on 200 randomly selected adults who were alive before the zombies. We have recent data for the 200 on age and sex, how many are in their household, and their rural, suburban, or urban location. Our heroic volunteers visit each home and record zombie status and preparedness. Now it's our job to figure out which supplies are associated with safety!

Acknowledgements
DataCamp

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43786) of an [OpenML dataset](https://www.openml.org/d/43786). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43786/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43786/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43786/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

